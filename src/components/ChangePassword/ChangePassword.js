import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import {reduxForm, reset} from 'redux-form';
import changePasswordValidation from './changePasswordValidation';
import { changePassword as changePasswordSubmit } from 'redux/modules/auth';

@reduxForm({
  form: 'ChangePassword',
  fields: ['password', 'newPassword', 'confirmPassword'],
  validate: changePasswordValidation,
})
@connect(
    () => ({
    }), {changePasswordSubmit, reset})
export default class ChangePassword extends Component {
  static propTypes = {
    changePasswordSubmit: PropTypes.func,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func,
    reset: PropTypes.func,
  }

  constructor() {
    super();
    this.state = {
      message: ''
    };
  }

  handleSubmit = (data) => {
    this.props.changePasswordSubmit(data).then((res) => {
      if (res.error) {
        this.setState({
          message: 'Error: Password not Correct'
        });
      } else {
        this.setState({
          message: 'Saved!'
        });
      }
    });
    this.props.reset('ChangePassword');
  }

  render() {
    const {
      fields: {password, newPassword, confirmPassword}
    } = this.props;

    const styles = require('./changePassword.scss');
    const renderInput = (field, label, showAsyncValidating, type) =>
      <div className={'form-group' + (field.error && field.touched ? ' has-error' : '')}>
        <label htmlFor={field.name} className="col-sm-2">{label}</label>
        <div className={'col-sm-8 ' + styles.inputGroup}>
          <input type={type || 'text'} className="form-control" id={field.name} {...field}/>
          {field.error && field.touched && <div className="text-danger">{field.error}</div>}
        </div>
      </div>;

    return (
      <div>
        <form className="form-horizontal" onSubmit={this.props.handleSubmit(this.handleSubmit)}>
          {renderInput(password, 'Current Password', undefined, 'password')}
          {renderInput(newPassword, 'New Password', undefined, 'password')}
          {renderInput(confirmPassword, 'Confirm Password', undefined, 'password')}
          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
              <button className="btn btn-success">
                <i className="fa fa-paper-plane"/> Submit
              </button>
            </div>
          </div>
        </form>
        {this.state.message}
      </div>
    );
  }
}
