import memoize from 'lru-memoize';
import {createValidator, required, minLength, match} from 'utils/validation';

const changePasswordValidation = createValidator({
  password: [required, minLength(7)],
  newPassword: [required, minLength(7)],
  confirmPassword: [required, minLength(7), match('newPassword')]
});
export default memoize(10)(changePasswordValidation);
