import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import {reduxForm} from 'redux-form';
import forgotChangePasswordValidation from './ForgotPasswordChangeFormValidation';

@reduxForm({
  form: 'ForgotPasswordChange',
  fields: ['password', 'confirmPassword'],
  validate: forgotChangePasswordValidation,
})
@connect(
    () => ({
    }), {})
export default class ForgotChangePassword extends Component {
  static propTypes = {
    forgotPasswordChange: PropTypes.func,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func,
  }

  constructor(props) {
    super(props);
  }

  render() {
    const {
      fields: {password, confirmPassword}
    } = this.props;

    const styles = require('./ForgotPasswordChangeForm.scss');
    const renderInput = (field, label, showAsyncValidating, type) =>
      <div className={'form-group' + (field.error && field.touched ? ' has-error' : '')}>
        <label htmlFor={field.name} className="col-sm-2">{label}</label>
        <div className={'col-sm-8 ' + styles.inputGroup}>
          <input type={type || 'text'} className="form-control" id={field.name} {...field}/>
          {field.error && field.touched && <div className="text-danger">{field.error}</div>}
        </div>
      </div>;

    return (
      <div>
        <form className="form-horizontal" onSubmit={this.props.handleSubmit}>
          {renderInput(password, 'New Password', undefined, 'password')}
          {renderInput(confirmPassword, 'Confirm Password', undefined, 'password')}
          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
              <button className="btn btn-success">
                <i className="fa fa-paper-plane"/> Submit
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
