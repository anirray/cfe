import memoize from 'lru-memoize';
import {createValidator, required, minLength, match} from 'utils/validation';

const forgotPasswordChangeFormValidation = createValidator({
  password: [required, minLength(7)],
  confirmPassword: [required, minLength(7), match('password')]
});
export default memoize(10)(forgotPasswordChangeFormValidation);
