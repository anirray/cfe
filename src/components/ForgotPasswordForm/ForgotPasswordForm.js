import React, { Component, PropTypes } from 'react';
import {reduxForm} from 'redux-form';
import forgotPasswordValidation from './forgotPasswordValidation';

@reduxForm({
  form: 'ForgotPasswordForm',
  fields: ['email'],
  validate: forgotPasswordValidation,
})
export default class ChangePassword extends Component {
  static propTypes = {
    changePasswordSubmit: PropTypes.func,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func,
  }

  constructor() {
    super();
    this.state = {
      message: ''
    };
  }

  render() {
    const {
      fields: {email}
    } = this.props;

    const styles = require('./ForgotPasswordForm.scss');
    const renderInput = (field, label, showAsyncValidating, type) =>
      <div className={'form-group' + (field.error && field.touched ? ' has-error' : '')}>
        <label htmlFor={field.name} className="col-sm-2">{label}</label>
        <div className={'col-sm-8 ' + styles.inputGroup}>
          <input type={type || 'text'} className="form-control" id={field.name} {...field}/>
          {field.error && field.touched && <div className="text-danger">{field.error}</div>}
        </div>
      </div>;

    return (
      <div>
        <form className="form-horizontal" onSubmit={this.props.handleSubmit}>
          {renderInput(email, 'Email')}
          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
              <button className="btn btn-success">
                <i className="fa fa-paper-plane"/> Submit
              </button>
            </div>
          </div>
        </form>
        {this.state.message}
      </div>
    );
  }
}
