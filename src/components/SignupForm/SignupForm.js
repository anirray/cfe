import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import signupValidation from './signupValidation';
import { checkEmailAvailability as checkEmail } from 'redux/modules/auth';
import moment from 'moment-timezone';
import jstz from 'jstimezonedetect';

function asyncValidate(data, dispatch) {
  // TODO: figure out a way to move this to the server. need an instance of ApiClient
  if (!data.email) {
    return Promise.resolve({});
  }
  return new Promise((resolve, reject) => {
    const errors = {};

    const promises = [];
    promises.push(dispatch(checkEmail(data.email)));

    return Promise.all(promises).then((userObj) =>{
      if (typeof userObj[0].error === 'object') {
        errors.email = 'Email address already used';
        reject(errors);
      } else {
        resolve();
      }
    }).catch(() => {
      errors.email = 'Email address already used';
      reject(errors);
    });
  });
}

@reduxForm({
  form: 'signup',
  fields: ['firstname', 'lastname', 'password', 'phoneNumber', 'email', 'timezone'],
  validate: signupValidation,
  initialValues: {
    timezone: jstz.determine().name(),
  },
  asyncValidate,
  asyncBlurFields: ['email']
})
export default
class SignupForm extends Component {
  static propTypes = {
    asyncValidating: PropTypes.bool.isRequired,
    fields: PropTypes.object.isRequired,
    // dirty: PropTypes.bool.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    resetForm: PropTypes.func.isRequired,
    // invalid: PropTypes.bool.isRequired,
    // valid: PropTypes.bool.isRequired
  }

  render() {
    const {
      asyncValidating,
      fields: {firstname, lastname, password, phoneNumber, email, timezone},
      handleSubmit,
      resetForm,
      // invalid,
      // valid
      } = this.props;
    const styles = require('./SignupForm.scss');
    const renderInput = (field, label, showAsyncValidating, type) =>
      <div className={'form-group' + (field.error && field.touched ? ' has-error' : '')}>
        <label htmlFor={field.name} className="col-sm-2">{label}</label>
        <div className={'col-sm-8 ' + styles.inputGroup}>
          {showAsyncValidating && asyncValidating && <i className={'fa fa-cog fa-spin ' + styles.cog}/>}
          <input type={type || 'text'} className="form-control" id={field.name} {...field}/>
          {field.error && field.touched && <div className="text-danger">{field.error}</div>}
        </div>
      </div>;

    return (
      <div>
        <form className="form-horizontal" onSubmit={handleSubmit}>
          {renderInput(firstname, 'First Name')}
          {renderInput(lastname, 'Last Name')}
          {renderInput(email, 'Email', true)}
          {renderInput(password, 'Password', undefined, 'password')}
          {renderInput(phoneNumber, 'Phone Number')}
          <div className={'form-group'}>
          <label className="col-sm-2">Timezone:</label>
            <div className={'col-sm-8'}>
              <select value={jstz.determine().name()} className="form-control" {...timezone}>
                {moment.tz.names().map(timeZone => <option key={timeZone} value={timeZone}>{timeZone}</option>)}
              </select>
            </div>
          </div>

          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
              <button className="btn btn-success" onClick={handleSubmit}>
                <i className="fa fa-paper-plane"/> Submit
              </button>
              <button className="btn btn-warning" onClick={resetForm} style={{marginLeft: 15}}>
                <i className="fa fa-undo"/> Reset
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

