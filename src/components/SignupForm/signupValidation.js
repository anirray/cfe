import memoize from 'lru-memoize';
import {createValidator, required, maxLength, email, integer, minLength} from 'utils/validation';

const signupValidation = createValidator({
  firstname: [required, minLength(1), maxLength(30)],
  email: [required, email],
  password: [required, minLength(7)],
  phoneNumber: [required, integer],
  lastname: [required, maxLength(20)] // single rules don't have to be in an array
});
export default memoize(10)(signupValidation);
