require('babel/polyfill');

const environment = {
  development: {
    isProduction: false
  },
  production: {
    isProduction: true
  }
}[process.env.NODE_ENV || 'development'];

module.exports = Object.assign({
  host: process.env.HOST || 'localhost',
  port: process.env.PORT,
  apiHost: process.env.APIHOST || 'localhost',
  apiPort: process.env.APIPORT,
  app: {
    title: 'Cognito',
    description: 'Find, Connect, Learn',
    meta: {
      charSet: 'utf-8',
      property: {
        'og:site_name': 'Cognito',
        'og:image': 'https://react-redux.herokuapp.com/logo.jpg',
        'og:locale': 'en_US',
        'og:title': 'Cognito',
        'og:description': 'Find, Connect, Learn',
        'twitter:card': 'summary',
        'twitter:site': '@faolainnyc',
        'twitter:creator': '@faolainnyc',
        'twitter:title': 'Cognito',
        'twitter:description': 'Find, Connect, Learn',
        'twitter:image': 'https://react-redux.herokuapp.com/logo.jpg',
        'twitter:image:width': '200',
        'twitter:image:height': '200'
      }
    }
  }
}, environment);
