import React, {Component, PropTypes} from 'react';
import { Link } from 'react-router';
import {connect} from 'react-redux';
// import connectData from 'helpers/connectData';

// function fetchDataDeferred(getState, dispatch, location, params) {
//   // return dispatch(loadUser(params.id));
//   return;
// }

// @connectData(null, fetchDataDeferred)
@connect(
    state => ({
      user: state.auth.user,
    }), {})
export default
class Account extends Component {
  static propTypes = {
    children: React.PropTypes.element,
    user: PropTypes.object,
  }

  render() {
    const styles = require('./Account.scss');
    const { user } = this.props;
    return (
      <div className={styles.account + ' ' + 'container'}>
          <h1> These are your account settings.</h1>
          <div><p> Please Select a tab below to edit your account settings </p></div>
          <div className="col-md-2">
            <ul>
              <li><Link to={`/account/accountSettings`} activeClassName={styles.active}>Account Settings</Link></li>
              {user.AccountTypeId === 2 &&
                <li><Link to={`/account/advisorSettings`} activeClassName={styles.active}>Advisor Settings</Link></li>
              }
              {user.AccountTypeId !== 2 &&
                <li><Link to={`/account/becomeAdvisor`} activeClassName={styles.active}>Become an Advisor</Link></li>
              }
              <li><Link to={`/account/calendarSettings`} activeClassName={styles.active}>Calendar Settings</Link></li>
              <li><Link to={`/account/changePassword`} activeClassName={styles.active}>Change Password</Link></li>
              <li><Link to={`/account/profileDisplay`} activeClassName={styles.active}>Profile Display</Link></li>
              <li><Link to={`/account/scheduler`} activeClassName={styles.active}>Scheduler</Link></li>
            </ul>
          </div>
          <div className="col-md-10">
            {this.props.children}
          </div>
      </div>
    );
  }
}
