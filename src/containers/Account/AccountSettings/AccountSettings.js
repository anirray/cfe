import React, { Component, PropTypes } from 'react';
import { confirmPin, changeEmail, changePhone } from 'redux/modules/auth';
import {connect} from 'react-redux';

@connect(
    state => ({
      user: state.auth.user,
    }), { confirmPin, changeEmail, changePhone })
export default
class AccountSettingsContainer extends Component {
  static propTypes = {
    changeEmail: PropTypes.func,
    changePhone: PropTypes.func,
    confirmPin: PropTypes.func,
    user: PropTypes.object,
  }

  constructor(props) {
    super(props);
    this.state = {
      emailSame: true,
      editingEmail: false,
      phoneSame: true,
      editingPhone: false,
      phoneError: ''
    };
  }

  confirmPin = (event) => {
    event.preventDefault();
    const pin = this.refs.pin.value;
    this.props.confirmPin(pin);
  }

  handleEmailEdit = () => {
    this.setState({
      editingEmail: true,
    });
  }

  handleEmailChange = () => {
    if (this.refs.email.value === this.props.user.email) {
      this.setState({
        emailSame: true
      });
    } else {
      this.setState({
        emailSame: false
      });
    }
  }

  handleEmailCancel = () => {
    this.setState({
      editingEmail: false,
    });
  }

  handleEmailSave = () => {
    if (this.refs.email.value !== this.props.user.email) {
      this.props.changeEmail(this.refs.email.value).then((data) => {
        if (!data.error) {
          this.setState({
            editingEmail: false
          });
        }
      });
    }
  }

  handlePhoneEdit = () => {
    this.setState({
      editingPhone: true,
    });
  }

  handlePhoneChange = () => {
    if (this.refs.phone.value === this.props.user.phoneNumber) {
      this.setState({
        phoneSame: true
      });
    } else {
      this.setState({
        phoneSame: false
      });
    }
  }

  handlePhoneSave = () => {
    if (this.refs.phone.value !== this.props.user.phoneNumber) {
      this.props.changePhone(this.refs.phone.value).then((data) => {
        if (!data.error) {
          this.setState({
            editingPhone: false
          });
        } else {
          this.setState({
            phoneError: data.error.message
          });
        }
      });
    }
  }

  handlePhoneCancel = () => {
    this.setState({
      editingPhone: false,
    });
  }

  render() {
    const { user } = this.props;
    const styles = require('./AccountSettings.scss');
    return (
      <div className={styles.accountSettings}>
        <div>
          <div>
           <label htmlFor="email">Email:</label>
           {this.state.editingEmail
             ?
             <div>
              <input onChange={this.handleEmailChange} ref="email" type="text" defaultValue={user.email} />
              <button onClick={this.handleEmailCancel}>Cancel</button>
              {!this.state.emailSame &&
                <button onClick={this.handleEmailSave}>Save</button>
              }
             </div>
             :
             <div>
               <span id="email"> {user.email} </span>
               <button onClick={this.handleEmailEdit}> Edit </button>
             </div>
           }
           <br />
           {!user.confirmEmail &&
            <span> <i className="fa fa-times" aria-hidden="true"></i> Email not confirmed </span>
           }
        </div>
        <br />
        <div>
           <label htmlFor="phonenumber">Phone Number:</label>

           {this.state.editingPhone
             ?
             <div>
              <input onChange={this.handlePhoneChange} ref="phone" type="text" defaultValue={user.phoneNumber} />
              <button onClick={this.handlePhoneCancel}>Cancel</button>
              {!this.state.phoneSame &&
                <button onClick={this.handlePhoneSave}>Save</button>
              }
             </div>
             :
             <div>
               <span id="phonenumber"> {user.phoneNumber} </span>
               <button onClick={this.handlePhoneEdit}> Edit </button>
             </div>
           }
           {!user.phoneConfirmation &&
             <span><i className="fa fa-times" aria-hidden="true"></i> Make this read and hoverable which says Phone Number not confirmed</span>
           }
           {!user.phoneConfirmation &&
            <form className="form-inline" onSubmit={this.confirmPin}>
              <label htmlFor="pin"> Confirm Pin: </label>
              <input type="text" ref="pin" id="pin"></input>
              <button>Submit</button>
            </form>
           }
           <div>
            <span className="text-danger"> {this.state.phoneError} </span>
           </div>
        </div>
       </div>
      </div>
    );
  }
}
