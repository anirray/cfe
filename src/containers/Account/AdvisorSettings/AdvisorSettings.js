import React, { Component, PropTypes } from 'react';
import { updateProfile } from 'redux/modules/auth';
import {connect} from 'react-redux';

@connect(
  state => ({
    user: state.auth.user,
  }), { updateProfile })
export default
class AccountSettingsContainer extends Component {
  static propTypes = {
    updateProfile: PropTypes.func,
    user: PropTypes.object,
  }

  constructor() {
    super();
    this.state = {
      editingRate: false,
      rateSame: true,
    };
  }

  handleRateEdit = () => {
    this.setState({
      editingRate: true,
    });
  }

  handleRateChange = () => {
    console.log('1');
    if (+this.refs.rate.value === this.props.user.rate) {
      this.setState({
        rateSame: true
      });
    } else {
      this.setState({
        rateSame: false
      });
    }
  }

  handleRateCancel = () => {
    this.setState({
      editingRate: false,
    });
  }

  handleRateSave = () => {
    const rate = this.refs.rate.value;
    if (rate !== this.props.user.rate) {
      this.props.updateProfile({rate}).then((data) => {
        if (!data.error) {
          this.setState({
            editingRate: false
          });
        }
      });
    }
  }

  render() {
    const { user } = this.props;
    const styles = require('./AdvisorSettings.scss');
    return (
      <div className={styles.advisorSettings}>
        <div>
           <label htmlFor="rate">Rate($/per minute):</label>
           {this.state.editingRate
             ?
             <div>
             <input onChange={this.handleRateChange} type="number" id="rate" defaultValue={user.rate} ref="rate"/>
              <button onClick={this.handleRateCancel}>Cancel</button>
              {!this.state.rateSame &&
                <button onClick={this.handleRateSave}>Save</button>
              }
             </div>
             :
             <div>
               <span id="rate"> {user.rate} </span>
               <button onClick={this.handleRateEdit}> Edit </button>
             </div>
           }
        </div>
        <br />
        <div>
          <span><strong>Set Availability:</strong></span>
          <br />
          <div> (Insert Calendar Component Here)</div>
        </div>
      </div>
    );
  }
}
