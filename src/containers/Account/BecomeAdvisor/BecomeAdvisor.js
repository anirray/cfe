import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import { pushState } from 'redux-router';
import { becomeAdvisor } from 'redux/modules/auth';

@connect(
    state => ({
      user: state.auth.user,
    }), {becomeAdvisor, pushState})
export default
class BecomeAdvisorContainer extends Component {
  static propTypes = {
    becomeAdvisor: PropTypes.func,
    pushState: PropTypes.func,
    user: PropTypes.object,
  }
  constructor() {
    super();
    this.state = {
      becomeAdvisor: false
    };
  }

  // componentDidMount() {
  //   // Integrate with google maps geolocation to find nearest city?
  //   const successFunction = ({coords: { latitude, longitude}}) => {
  //     console.log('lat long', latitude, longitude);
  //   };
  //
  //   const errorFunction = () => {
  //     console.log('error');
  //   };
  //
  //   if (navigator.geolocation) {
  //     navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
  //   }
  // }

  becomeAdvisor = () => {
    this.setState({
      becomeAdvisor: true
    });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.props.becomeAdvisor(event.target[0].value).then((data) => {
      if (!data.error) {
        this.props.pushState(null, `/account/advisorSettings`);
      }
    });
  }

  render() {
    const { user } = this.props;
    const styles = require('./BecomeAdvisor.scss');
    return (
      <div className={styles.becomeAdvisor}>
          {this.state.becomeAdvisor
            ? <div>
                <div>
                  <form onSubmit={this.handleSubmit}>
                    Rate: <input type="number" />
                    <button onSubmit={this.handleSubmit}> Submit </button>
                  </form>
                </div>
              </div>
            :
            <div>
              {(user.phoneConfirmation && user.confirmEmail) ?
                <button onClick={this.becomeAdvisor}> Become Advisor! </button>
                : <span> Please confirm your email and phone number before becoming an advisor </span>
              }
            </div>
          }
      </div>
    );
  }
}
