import React, { Component, PropTypes } from 'react';
import moment from 'moment-timezone';
import {connect} from 'react-redux';

@connect(
    state => ({
      user: state.auth.user,
    }), {})
export default
class CalendarSettings extends Component {
  static propTypes = {
    user: PropTypes.object,
  }

  render() {
    const { user } = this.props;
    const styles = require('./CalendarSettings.scss');
    return (
      <div className={styles.calendarSettings}>
        <div>
          <span> Timezone: </span>
          <br />

          <select defaultValue={user.timezone}>
          {moment.tz.names().map(timeZone => <option key={timeZone} value={timeZone}>{timeZone}</option>)}
        </select>
        <br />
        <br />
        </div>
        <span>View Format:</span>
        <div>
          <form>
            <input type="radio" name="calendarView" value="12" checked />12hr<br />
             <input type="radio" name="calendarView" value="24" />24hr
          </form>
        </div>
        <button> Update Calendar </button>
        <br />
        <br />
        <div><button> Sync Google Calendar </button></div>
      </div>
    );
  }
}
