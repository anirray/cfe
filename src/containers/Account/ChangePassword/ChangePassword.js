import React, { Component } from 'react';
import { ChangePassword } from 'components';

export default
class ChangePasswordContainer extends Component {

  render() {
    const styles = require('./ChangePassword.scss');
    return (
      <div className={styles.changePassword}>
        <ChangePassword />
      </div>
    );
  }
}
