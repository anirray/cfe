import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import { updateProfile } from 'redux/modules/auth';
import {Scheduler} from 'containers';

@connect(
    state => ({
      user: state.auth.user,
    }), { updateProfile })
export default
class ProfileDisplayContainer extends Component {
  static propTypes = {
    updateProfile: PropTypes.func,
    user: PropTypes.object,
  }

  constructor() {
    super();
    this.state = {
      error: '',
      success: ''
    };
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const firstname = this.refs.firstname.value;
    const lastname = this.refs.lastname.value;
    this.props.updateProfile({firstname, lastname}).then((data) => {
      if (!data.error) {
        this.setState({
          success: 'Submmited!'
        });
      } else {
        this.setState({
          error: data.error.message,
          success: ''
        });
      }
    });
  }

  render() {
    const { user } = this.props;
    const styles = require('./ProfileDisplay.scss');
    return (
      <div className={styles.profileDisplay}>
        <form>
          <div>
             <label htmlFor="firstname">First Name:</label>
             <input type="text" defaultValue={user.firstname} id="firstname" ref="firstname"/>
          </div>
          <br />
          <div>
             <label htmlFor="lastname">Last Name:</label>
             <input type="text" defaultValue={user.lastname} id="lastname" ref="lastname"/>
          </div>
          <br />
          <button onClick={this.handleSubmit}> Update Profile </button>
        </form>
        {this.state.success || this.state.error}
        <Scheduler settingTimes={false} profile={user} />
      </div>
    );
  }
}
