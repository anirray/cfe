import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router';
// import connectData from 'helpers/connectData';

// function fetchDataDeferred(getState, dispatch, location, params) {
//   // return dispatch(loadUser(params.id));
//   return;
// }

// @connectData(null, fetchDataDeferred)
@connect(
    () => ({
    }), {})
export default
class Admin extends Component {
  static propTypes = {
    children: React.PropTypes.element,
    user: PropTypes.object,
  }

  render() {
    const styles = require('./Admin.scss');
    console.log(styles);
    return (
      <div className={styles.admin}>
        <h1> Admin Panel </h1>
        <div className="container">
          <div className="col-md-3">
            <ul>
              <li><Link to={`/admin/categories`} activeClassName={styles.active}>Categories</Link></li>
              <li><Link to={`/admin/roles`} activeClassName={styles.active}>Roles</Link></li>
              <li><Link to={`/admin/users`} activeClassName={styles.active}>User Administration</Link></li>
            </ul>
          </div>
          <div className="col-md-9">
            {this.props.children}
          </div>
        </div>
      </div>
    );
  }
}
