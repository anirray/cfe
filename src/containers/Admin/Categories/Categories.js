import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import { loadCategories, addCategory, editStart } from 'redux/modules/categories';
import connectData from 'helpers/connectData';
import { CategoryAdminForm } from 'components';

function fetchDataDeferred(getState, dispatch) {
  return dispatch(loadCategories());
}

@connectData(null, fetchDataDeferred)
@connect(
    (state) => ({
      categories: state.categories.categories,
      editing: state.categories.editing,
    }), { addCategory, editStart})
export default
class Categories extends Component {
  static propTypes = {
    addCategory: PropTypes.func,
    categories: PropTypes.array,
    editing: PropTypes.object.isRequired,
    editStart: PropTypes.func.isRequired,
    user: PropTypes.object,
  }

  addCategory = (event) => {
    event.preventDefault();
    const category = this.refs.category.value;
    this.props.addCategory(category);
    console.log(category);
  }

  render() {
    const handleEdit = (category) => {
      const {editStart} = this.props; // eslint-disable-line no-shadow
      return () => editStart(String(category.id));
    };

    const styles = require('./Categories.scss');
    const { categories, editing } = this.props;

    return (
      <div className={styles.categories}>
          <label>Create a New Category</label>
          <form onSubmit={this.addCategory}>
            <input ref="category" type="text"></input>
            <br />
            <button>Submit</button>
          </form>
          <br />
          <br />
          <h4>List of Categories</h4>
          {categories && categories.length &&
          <table className="table table-striped">
            <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
            </tr>
            </thead>
            <tbody>
            {
              categories.map((category) => editing[category.id] ?
                <CategoryAdminForm formKey={String(category.id)} key={String(category.id)} initialValues={category}/> :
                <tr key={category.id}>
                  <td>{category.id}</td>
                  <td>{category.name}</td>
                  <td>
                    <button className="btn btn-primary" onClick={handleEdit(category)}>
                      <i className="fa fa-pencil"/> Edit
                    </button>
                  </td>
                </tr>)
            }
            </tbody>
          </table>}
      </div>
    );
  }
}
