import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import { loadAdvisors } from 'redux/modules/advisors';
import connectData from 'helpers/connectData';
import { Link } from 'react-router';

function fetchDataDeferred(getState, dispatch) {
  return dispatch(loadAdvisors());
}

@connectData(null, fetchDataDeferred)
@connect(
    state => ({
      advisors: state.advisors.advisors,
    }), {})
export default class AdvisorList extends Component {
  static propTypes = {
    advisors: PropTypes.array,
  }
  render() {
    const styles = require('./AdvisorList.scss');
    let {advisors} = this.props;

    advisors = advisors.map((advisor) => {
      if (advisor.AccountTypeId === 2 && advisor.confirmEmail && advisor.phoneConfirmation) {
        return (
          <li key={advisor.id}><Link to={`/profile/${advisor.id}`}>{advisor.firstname} {advisor.lastname}</Link></li>
        );
      }
      return null;
    });

    return (
      <div className={'container' + ' ' + styles.advisorList}>
        <h1> Advisor Listing </h1>

        <div className="container">
          <ul>
            {advisors}
          </ul>
        </div>
      </div>
    );
  }
}
