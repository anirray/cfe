import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { IndexLink, Link } from 'react-router';
import { LinkContainer } from 'react-router-bootstrap';
import { Navbar, NavBrand, Nav, NavItem, CollapsibleNav, Alert } from 'react-bootstrap';
import DocumentMeta from 'react-document-meta';
import { isLoaded as isInfoLoaded, load as loadInfo } from 'redux/modules/info';
import { load as loadAuth, logout, resendEmailConfirmation } from 'redux/modules/auth';
import { InfoBar } from 'components';
import { pushState } from 'redux-router';
import connectData from 'helpers/connectData';
import config from '../../config';

function fetchData(getState, dispatch) {
  const promises = [];
  if (!isInfoLoaded(getState())) {
    promises.push(dispatch(loadInfo()));
  }
  // if (!isAuthLoaded(getState())) {
  //   promises.push(dispatch(loadAuth()));
  // }
  promises.push(dispatch(loadAuth()));
  return Promise.all(promises);
}

@connectData(fetchData)
@connect(
  state => ({
    confirmed: state.auth.confirmed,
    user: state.auth.user,
    resendEmailConfirmationStatus: state.auth.resendEmailConfirmationStatus,
  }),
  {logout, pushState, resendEmailConfirmation})
export default class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
    confirmed: PropTypes.bool,
    user: PropTypes.object,
    logout: PropTypes.func.isRequired,
    pushState: PropTypes.func.isRequired,
    resendEmailConfirmation: PropTypes.func.isRequired,
    resendEmailConfirmationStatus: PropTypes.bool,
  };

  static contextTypes = {
    store: PropTypes.object.isRequired
  };

  componentWillReceiveProps(nextProps) {
    if (!this.props.user && nextProps.user) {
      // login
      this.props.pushState(null, `/profile/${nextProps.user.id}`);
    } else if (this.props.user && !nextProps.user) {
      // logout
      this.props.pushState(null, '/');
    }
  }

  handleLogout = (event) => {
    event.preventDefault();
    this.props.logout();
  }

  render() {
    const {user, confirmed} = this.props;
    const styles = require('./App.scss');
    return (
      <div className={styles.app}>
        <DocumentMeta {...config.app}/>
        <Navbar fixedTop toggleNavKey={0}>
          <NavBrand>
            <IndexLink to="/" activeStyle={{color: '#33e0ff'}}>
              <div className={styles.brand}/>
              <span>{config.app.title}</span>
            </IndexLink>
          </NavBrand>

          <CollapsibleNav eventKey={0}>
            <Nav navbar>
              {user && <LinkContainer to="/chat">
                <NavItem eventKey={1}>Chat</NavItem>
              </LinkContainer>}

              {user && <LinkContainer to="/orders">
                <NavItem eventKey={8}>Orders</NavItem>
              </LinkContainer>}

              <LinkContainer to="/widgets">
                <NavItem eventKey={2}>Widgets</NavItem>
              </LinkContainer>
              <LinkContainer to="/survey">
                <NavItem eventKey={3}>Survey</NavItem>
              </LinkContainer>
              <LinkContainer to="/about">
                <NavItem eventKey={4}>About Us</NavItem>
              </LinkContainer>

              <LinkContainer to="/advisorlist">
                <NavItem eventKey={7}>Advisor List</NavItem>
              </LinkContainer>

              {!user &&
              <LinkContainer to="/login">
                <NavItem eventKey={5}>Login</NavItem>
              </LinkContainer>}
              {user &&
              <LinkContainer to="/logout">
                <NavItem eventKey={6} className="logout-link" onClick={this.handleLogout}>
                  Logout
                </NavItem>
              </LinkContainer>}
            </Nav>

            {user &&
            <p className={styles.loggedInMessage + ' navbar-text'}>Logged in as <Link to="/account"><strong>{user.email}</strong>.</Link></p>
            }
            <Nav navbar right>
              <NavItem eventKey={1} target="_blank" title="View on Github" href="https://github.com/erikras/react-redux-universal-hot-example">
                <i className="fa fa-github"/>
              </NavItem>
            </Nav>
          </CollapsibleNav>
        </Navbar>

        <div className={styles.appContent}>
          {user && !user.confirmEmail && !confirmed &&
            <Alert bsStyle="warning">
            {this.props.resendEmailConfirmationStatus && <div> <strong>Email confirmation sent</strong></div>}
            {!this.props.resendEmailConfirmationStatus && <div><strong>Please confirm your email address</strong> <button onClick={this.props.resendEmailConfirmation}>Resend confirmation link</button></div>}
          </Alert>
          }
          {this.props.children}
        </div>
        <InfoBar/>

        <div className="well text-center">
          Copyright © 2016 Cognito
        </div>
      </div>
    );
  }
}
