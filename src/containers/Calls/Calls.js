import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import { loadCalls } from 'redux/modules/calls';
import connectData from 'helpers/connectData';
import { pushState } from 'redux-router';

function fetchDataDeferred(getState, dispatch) {
  return dispatch(loadCalls());
}

@connectData(null, fetchDataDeferred)
@connect(
    state => ({
      calls: state.calls.calls,
    }), { pushState })
export default class Calls extends Component {
  static propTypes = {
    calls: PropTypes.array,
    pushState: PropTypes.func,
  }

  handleLink = (id) => {
    this.props.pushState('null', `/call/${id}`);
  }

  render() {
    const styles = require('./Calls.scss');
    let { calls } = this.props;

    calls = calls.map((call) => {
      return (
        <tr onClick={this.handleLink.bind(null, call.id)}>
          <td> {call.bookingID} </td>
          <td> {call.duration} </td>
          <td> {call.status} </td>
          <td> {call.updatedAt} </td>
          <td> {call.createdAt} </td>
          <td> {call.AdvisorId} </td>
          <td> {call.userId} </td>
        </tr>
      );
    });

    return (
      <div className={'container' + ' ' + styles.calls}>
        <h1> Call List </h1>
        <div className="container">
          <table>
            <thead>
              <tr>
                <th> Booking ID </th>
                <th> Duration </th>
                <th> Status </th>
                <th> Last Updated </th>
                <th> Date Booked </th>
                <th> Advisor Id </th>
                <th> User ID </th>
              </tr>
            </thead>
            <tbody>
              {calls}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
