import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import { pushState } from 'redux-router';
import {reset} from 'redux-form';
import { ForgotPasswordChangeForm } from 'components';

import { checkForgotPassword, forgotPasswordChange } from 'redux/modules/auth';
import connectData from 'helpers/connectData';

function fetchDataDeferred(getState, dispatch, location) {
  return dispatch(checkForgotPassword(location.query.token));
}

@connectData(null, fetchDataDeferred)
@connect(
    (state) => ({
      confirmedPassword: state.auth.confirmedPassword
    }),
    {forgotPasswordChange, reset, pushState}
)
export default
class ConfirmPassword extends Component {
  static propTypes = {
    confirmedPassword: PropTypes.bool,
    forgotPasswordChange: PropTypes.func,
    location: PropTypes.object,
    reset: PropTypes.func,
    pushState: PropTypes.func.isRequired,
  }

  constructor() {
    super();
    this.state = {
      message: ''
    };
  }

  componentDidMount() {
  }

  handleSubmit = (data) => {
    const token = this.props.location.query.token;
    this.props.forgotPasswordChange(token, data).then((res) => {
      if (res.error) {
        this.setState({
          message: 'We apologize but an error was encountered please try again later'
        });
      } else {
        this.setState({
          message: 'Saved!'
        });
      }
    });
    this.props.reset('ForgotPasswordChange');
  }

  render() {
    const { confirmedPassword } = this.props;

    return (
      <div className="container">
      {confirmedPassword &&
        <div>
          <h1>CHange your Password</h1>
          <div>
            <ForgotPasswordChangeForm onSubmit={this.handleSubmit}/>
            {this.state.message}
          </div>
        </div>
      }
      {!confirmedPassword &&
          <div>
            <h1>Invalid Token</h1>
            <div>
            Your link was invalid, it may have expired or have been incorrectly typed.
            </div>
          </div>
      }
    </div>
    );
  }
}
