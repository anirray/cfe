import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import { pushState } from 'redux-router';

import { checkEmailConfirmation } from 'redux/modules/auth';
import connectData from 'helpers/connectData';

function fetchDataDeferred(getState, dispatch, location) {
  return dispatch(checkEmailConfirmation(location.query.token));
}

@connectData(null, fetchDataDeferred)
@connect(
    state => ({
      confirmed: state.auth.confirmed,
    }),
    {pushState}
)
export default
class ConfirmUser extends Component {
  static propTypes = {
    confirmed: PropTypes.bool,
    pushState: PropTypes.func.isRequired,
  }

  componentDidMount() {
    if (this.props.confirmed) {
      setTimeout(() =>{
        this.props.pushState(null, `/login`);
      }, 4000);
    } else {
      setTimeout(() =>{
        this.props.pushState(null, `/signup`);
      }, 4000);
    }
  }

  render() {
    const { confirmed } = this.props;

    return (
      <div className="container">
      {confirmed &&
        <div>
          <h1>Confirmed!</h1>
          <div>
            You have been confirmed and will be redirected to the login page.
          </div>
        </div>
      }
      {!confirmed &&
          <div>
            <h1>Invalid Token</h1>
            <div>
            Your link was invalid or the account may already have been activated, you will now be redirected to the signup page.
            </div>
          </div>
      }
    </div>
    );
  }
}
