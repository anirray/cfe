import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';

@connect(
    state => ({
      user: state.auth.user,
    }), {})
export default class Dashboard extends Component {
  static propTypes = {
    user: PropTypes.object,
  }

  render() {
    const styles = require('./Dashboard.scss');
    const { user } = this.props;

    return (
      <div className={'container' + ' ' + styles.dashboard}>
        <h1> Dashboard </h1>
        <div className="container">
            <span> Welcome {user.firstname} PUT DASHBOARD THINGS IN HERE</span>
        </div>
      </div>
    );
  }
}
