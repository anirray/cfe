import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {reset} from 'redux-form';
import { ForgotPasswordForm } from 'components';
import { forgotPassword } from 'redux/modules/auth';

@connect(
    state => ({
      user: state.auth.user,
    }),
    {reset, forgotPassword})
export default
class ForgotPassword extends Component {
  static propTypes = {
    forgotPassword: PropTypes.func,
    reset: PropTypes.func,
  }

  constructor(props) {
    super(props);
    this.state = {error: ''};
  }

  handleSubmit = (data) => {
    this.props.forgotPassword(data.email).then((res) => {
      if (res.error) {
        this.setState({
          error: res.error,
        });
      } else {
        this.setState({
          error: '',
        });
      }
    });
    this.props.reset('ForgotPasswordForm');
  }

  render() {
    const { error } = this.state;
    return (
      <div className="container">
        <h1> Forgot Password? </h1>
        <ForgotPasswordForm onSubmit={this.handleSubmit}/>
        {error &&
            <div className="alert alert-danger" role="alert">
              <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
              {' '}
              {error}
            </div>
        }
      </div>
    );
  }
}
