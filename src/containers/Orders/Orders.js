import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import { loadOrders } from 'redux/modules/orders';
import connectData from 'helpers/connectData';
// import { Link } from 'react-router';

function fetchDataDeferred(getState, dispatch) {
  return dispatch(loadOrders());
}

@connectData(null, fetchDataDeferred)
@connect(
    state => ({
      orders: state.orders.orders,
    }), {})
export default class AdvisorList extends Component {
  static propTypes = {
    orders: PropTypes.array,
  }
  render() {
    const styles = require('./Orders.scss');
    let {orders} = this.props;

    orders = orders.map((order) => {
      return (
        <tr>
          <td>{order.UserId} </td>
          <td></td>
          <td>{order.amount}</td>
          <td></td>
          <td></td>
        </tr>
      );
    });

    return (
      <div className={'container' + ' ' + styles.orders}>
        <h1> Order List </h1>
        <div className="container">
          <table>
            <thead>
              <tr>
                <th> Caller </th>
                <th> Duration </th>
                <th> Price </th>
                <th> Services </th>
                <th> Category </th>
              </tr>
            </thead>
            <tbody>
              {orders}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
