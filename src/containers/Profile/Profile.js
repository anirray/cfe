import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import * as authActions from 'redux/modules/auth';
import { load as loadUser } from 'redux/modules/profile';
import connectData from 'helpers/connectData';
import {Scheduler} from 'containers';
function fetchDataDeferred(getState, dispatch, location, params) {
  const promises = [];
  if (params.id) promises.push(dispatch(loadUser(params.id)));
  return Promise.all(promises);
}


@connectData(null, fetchDataDeferred)
@connect(
    state => ({
      user: state.auth.user,
      profile: state.profile.user
    }),
    {authActions, loadUser})
export default
class Profile extends Component {
  static propTypes = {
    user: PropTypes.object,
    logout: PropTypes.func,
    profile: PropTypes.object,
    loadUser: PropTypes.func
  }


  constructor(props) {
    super(props);
  }

  render() {
    const {user, profile} = this.props;

    if (user && profile && user.id === profile.id ) {
      return (
      <div className="container">
        <div>
          <h1>Your profile</h1>
          <Scheduler settingTimes={false} profile={profile} />
        </div>
      </div>
      );
    } else if (profile && (!user || user.id !== profile.id)) {
      return (
        <Scheduler settingTimes={false} profile={profile} />
        );
    } else if (!profile) {
      return (
         <div className="container">
          Profile not found
        </div>
      );
    }
  }
}
