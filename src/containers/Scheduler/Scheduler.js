import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import { Tabs, Tab } from 'react-bootstrap';
import moment from 'moment';
import * as authActions from 'redux/modules/auth';
import {setAvail} from 'redux/modules/profile';
// import connectData from 'helpers/connectData';
import {loadUserCalendar} from 'redux/modules/profile';
import {loadGlobalCalendar} from 'redux/modules/calendar';

// function fetchDataDeferred(getState, dispatch, location, params) {
//   const promises = [];
//   if (params.id) promises.push(dispatch(loadUserCalendar(params.id)));
//   promises.push(dispatch(loadGlobalCalendar()));
//   return Promise.all(promises);
// }

// @connectData(null, fetchDataDeferred)
@connect(
    (state) => ({
      globalCalendar: state.calendar.globalCalendar,
      userCalendar: state.profile.calendar,
      user: state.auth.user,
    }), {authActions, setAvail, loadGlobalCalendar, loadUserCalendar})
export default
class Scheduler extends Component {

  static propTypes = {
    user: PropTypes.object,
    setAvail: PropTypes.func,
    globalCalendar: PropTypes.array,
    calendar: PropTypes.array,
    viewOwnProfile: PropTypes.string,
    loadGlobalCalendar: PropTypes.func,
    userCalendar: PropTypes.array,
    settingTimes: PropTypes.bool,
    loadUserCalendar: PropTypes.func,
    profile: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {
      timeslots: [],
      days: []
    };
  }

  componentWillMount() {

    if (this.props.settingTimes === false) {
      this.props.loadUserCalendar(this.props.profile.id)
      .then((data) => {
        const userCalendar = data.result;
        const days = {};
        if (userCalendar.length !== 0) {
          for (let index = 0; index < userCalendar.length; index++) {
            if (days[moment(parseInt(userCalendar[index].utc, 10)).format('dddd, MMMM Do YYYY')] !== undefined) {
              days[moment(parseInt(userCalendar[index].utc, 10)).format('dddd, MMMM Do YYYY')].push(userCalendar[index]);
            } else {
              days[moment(parseInt(userCalendar[index].utc, 10)).format('dddd, MMMM Do YYYY')] = [];
              days[moment(parseInt(userCalendar[index].utc, 10)).format('dddd, MMMM Do YYYY')].push(userCalendar[index]);
            }
          }
          this.setState({
            timeslots: [],
            days: days
          });
        }
      });
    } else {
      this.props.loadGlobalCalendar()
      .then((data) =>{
        const globalCalendar = data.result;
        const days = {};
        for (let index = 0; index < globalCalendar.length; index++) {
          if (days[moment(parseInt(globalCalendar[index].utc, 10)).format('dddd, MMMM Do YYYY')] !== undefined) {
            days[moment(parseInt(globalCalendar[index].utc, 10)).format('dddd, MMMM Do YYYY')].push(globalCalendar[index]);
          } else {
            days[moment(parseInt(globalCalendar[index].utc, 10)).format('dddd, MMMM Do YYYY')] = [];
            days[moment(parseInt(globalCalendar[index].utc, 10)).format('dddd, MMMM Do YYYY')].push(globalCalendar[index]);
          }
        }

        this.setState({
          timeslots: [],
          days: days
        });
      });
    }
  }

  selectSlot = (event) => {
    this.setState({timeslots: this.state.timeslots.concat(event.target.value)});
  }

  submitSlots = () => {
    this.props.setAvail(this.state.timeslots, this.props.user);
  }


  render() {
    console.log('props', this.props);
    const styles = require('containers/Scheduler/Scheduler.scss');
    const daysMap = Object.keys(this.state.days).map((day, index) => {
      return (
      <Tab title={day} eventKey={index} key={index}><br></br>
          <table className="response-time-table disable-text-selection" cellSpacing="0" cellPadding="0">
          <tbody>
              <tr>
                  <td></td>
                  <td className="text-center"><h5>Available</h5></td>
              </tr>
              <tr>
                  <td></td>
                  <td className="text-center"><button onClick={this.submitSlots}>Submit</button></td>
              </tr>
              {this.state.days[day].map((time, index2) => {
                const selected = this.state.timeslots.indexOf(time.utc) === -1 ? '' : styles.able;
                return (
                  <tr key={index2}>
                    <td><p>{moment(parseInt(time.utc, 10)).format('hh:mm A')}</p></td>
                    <td className="text-center response-cell"><button onClick={this.selectSlot} value={time.utc} className={selected}></button></td>

                </tr>
              );
              })}
          </tbody>
          </table>
      </Tab>
    );
    });
    return (
        <Tabs defaultActiveKey={1} position="right" tabWidth={3}>
          {daysMap}
        </Tabs>
    );
  }
}
