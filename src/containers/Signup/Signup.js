import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import { pushState } from 'redux-router';
import { create } from 'redux/modules/auth';
// import DocumentMeta from 'react-document-meta';
// import { initialize } from 'redux-form';
import { SignupForm } from 'components';
// import config from '../../config';

@connect(
    state => ({
      user: state.auth.user,
      error: state.auth.error,
    }),
    {create, pushState})
export default
class Signup extends Component {
  static propTypes = {
    create: PropTypes.func.isRequired,
    error: PropTypes.object,
    user: PropTypes.object,
    pushState: PropTypes.func.isRequired,
  }

  componentDidMount() {
    if (this.props.user) {
      this.props.pushState(null, '/loginSuccess');
    }
  }

  handleSubmit = (data) => {
    this.props.create(data);
  }

  render() {
    const { error } = this.props;
    return (
      <div className="container">
        <h1> SIGN UP PAGE </h1>
        <SignupForm onSubmit={this.handleSubmit}/>
        {error &&
            <div className="alert alert-danger" role="alert">
              <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
              {' '}
              {error.message}
            </div>
        }
      </div>
    );
  }
}
