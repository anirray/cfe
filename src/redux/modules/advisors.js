const LOAD = 'redux-example/advisorList/LOAD';
const LOAD_SUCCESS = 'redux-example/advisorList/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/advisorList/LOAD_FAIL';

const initialState = {
  advisors: [],
  loaded: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        advisors: action.result
      };
    case LOAD_FAIL:
      return {
        ...state,
        user: null,
        loading: false,
        loaded: false,
        error: action.error
      };
    default:
      return state;
  }
}

export function loadAdvisors() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get('/users/loadUsers/')
  };
}
