const LOAD = 'redux-example/auth/LOAD';
const LOAD_SUCCESS = 'redux-example/auth/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/auth/LOAD_FAIL';
const BECOMEADVISOR = 'redux-example/auth/BECOMEADVISOR';
const BECOMEADVISOR_SUCCESS = 'redux-example/auth/BECOMEADVISOR_SUCCESS';
const BECOMEADVISOR_FAIL = 'redux-example/auth/BECOMEADVISOR_FAIL';
const LOGIN = 'redux-example/auth/LOGIN';
const LOGIN_SUCCESS = 'redux-example/auth/LOGIN_SUCCESS';
const LOGIN_FAIL = 'redux-example/auth/LOGIN_FAIL';
const LOGOUT = 'redux-example/auth/LOGOUT';
const LOGOUT_SUCCESS = 'redux-example/auth/LOGOUT_SUCCESS';
const LOGOUT_FAIL = 'redux-example/auth/LOGOUT_FAIL';
const CREATE = 'redux-example/auth/CREATE';
const CREATE_SUCCESS = 'redux-example/auth/CREATE_SUCCESS';
const CREATE_FAIL = 'redux-example/auth/CREATE_FAIL';
const CHANGEEMAIL = 'redux-example/auth/CHANGEEMAIL';
const CHANGEEMAIL_SUCCESS = 'redux-example/auth/CHANGEEMAIL_SUCCESS';
const CHANGEEMAIL_FAIL = 'redux-example/auth/CHANGEEMAIL_FAIL';
const CHECKEMAIL = 'redux-example/auth/CHECKEMAIL';
const CHECKEMAIL_SUCCESS = 'redux-example/auth/CHECKEMAIL_SUCCESS';
const CHECKEMAIL_FAIL = 'redux-example/auth/CHECKEMAIL_FAIL';
const CHECKEMAILCONFIRMATION = 'redux-example/auth/CHECKEMAILCONFIRMATION';
const CHECKEMAILCONFIRMATION_SUCCESS = 'redux-example/auth/CHECKEMAILCONFIRMATION_SUCCESS';
const CHECKEMAILCONFIRMATION_FAIL = 'redux-example/auth/CHECKEMAILCONFIRMATION_FAIL';
const RESENDEMAILCONFIRMATION = 'redux-example/auth/RESENDEMAILCONFIRMATION';
const RESENDEMAILCONFIRMATION_SUCCESS = 'redux-example/auth/RESENDEMAILCONFIRMATION_SUCCESS';
const RESENDEMAILCONFIRMATION_FAIL = 'redux-example/auth/RESENDEMAILCONFIRMATION_FAIL';
const CHANGEPASSWORD = 'redux-example/auth/CHANGEPASSWORD';
const CHANGEPASSWORD_SUCCESS = 'redux-example/auth/CHANGEPASSWORD_SUCCESS';
const CHANGEPASSWORD_FAIL = 'redux-example/auth/CHANGEPASSWORD_FAIL';
const FORGOTPASSWORD = 'redux-example/auth/FORGOTPASSWORD';
const FORGOTPASSWORD_SUCCESS = 'redux-example/auth/FORGOTPASSWORD_SUCCESS';
const FORGOTPASSWORD_FAIL = 'redux-example/auth/FORGOTPASSWORD_FAIL';
const CHECKFORGOTPASSWORD = 'redux-example/auth/CHECKFORGOTPASSWORD';
const CHECKFORGOTPASSWORD_SUCCESS = 'redux-example/auth/CHECKFORGOTPASSWORD_SUCCESS';
const CHECKFORGOTPASSWORD_FAIL = 'redux-example/auth/CHECKFORGOTPASSWORD_FAIL';
const FORGOTPASSWORDCHANGE = 'redux-example/auth/FORGOTPASSWORDCHANGE';
const FORGOTPASSWORDCHANGE_SUCCESS = 'redux-example/auth/FORGOTPASSWORDCHANGE_SUCCESS';
const FORGOTPASSWORDCHANGE_FAIL = 'redux-example/auth/FORGOTPASSWORDCHANGE_FAIL';
const CONFIRMPIN = 'redux-example/auth/CONFIRMPIN';
const CONFIRMPIN_SUCCESS = 'redux-example/auth/CONFIRMPIN_SUCCESS';
const CONFIRMPIN_FAIL = 'redux-example/auth/CONFIRMPIN_FAIL';
const CHANGEPHONE = 'redux-example/auth/CONFIRMPIN';
const CHANGEPHONE_SUCCESS = 'redux-example/auth/CHANGEPHONE_SUCCESS';
const CHANGEPHONE_FAIL = 'redux-example/auth/CHANGEPHONE_FAIL';
const UPDATEPROFILE = 'redux-example/auth/UPDATEPROFILE';
const UPDATEPROFILE_SUCCESS = 'redux-example/auth/UPDATEPROFILE_SUCCESS';
const UPDATEPROFILE_FAIL = 'redux-example/auth/UPDATEPROFILE_FAIL';

const initialState = {
  loaded: false,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        user: action.result
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    case LOGIN:
      return {
        ...state,
        loggingIn: true
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        loggingIn: false,
        user: action.result
      };
    case LOGIN_FAIL:
      return {
        ...state,
        loggingIn: false,
        user: null,
        loginError: action.error
      };
    case LOGOUT:
      return {
        ...state,
        loggingOut: true
      };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        loggingOut: false,
        user: null
      };
    case LOGOUT_FAIL:
      return {
        ...state,
        loggingOut: false,
        logoutError: action.error
      };
    case CREATE:
      return {
        ...state,
        loading: true
      };
    case CREATE_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        user: action.result
      };
    case CREATE_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    case CHECKEMAILCONFIRMATION:
      return {
        ...state,
        loading: true
      };
    case CHECKEMAILCONFIRMATION_SUCCESS:
      return {
        ...state,
        confirmed: true,
        loading: false,
      };
    case CHECKEMAILCONFIRMATION_FAIL:
      return {
        ...state,
        confirmed: false,
        loading: false,
      };
    case RESENDEMAILCONFIRMATION:
      return {
        ...state,
        loading: true,
      };
    case RESENDEMAILCONFIRMATION_SUCCESS:
      return {
        ...state,
        loading: false,
        resendEmailConfirmationStatus: true,
      };
    case RESENDEMAILCONFIRMATION_FAIL:
      return {
        ...state,
        loading: false,
        resendEmailConfirmationError: true,
      };
    case CHECKFORGOTPASSWORD:
      return {
        ...state,
        loading: true
      };
    case CHECKFORGOTPASSWORD_SUCCESS:
      return {
        ...state,
        confirmedPassword: true,
        loading: false,
      };
    case CHECKFORGOTPASSWORD_FAIL:
      return {
        ...state,
        confirmedPassword: false,
        loading: false,
      };
    case CONFIRMPIN:
      return {
        ...state,
        loading: true
      };
    case CONFIRMPIN_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        user: action.result
      };
    case CONFIRMPIN_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    case CHANGEEMAIL:
      return {
        ...state,
        changeEmailLoading: true,
      };
    case CHANGEEMAIL_SUCCESS:
      return {
        ...state,
        user: action.result,
        changeEmailLoading: false,
        changeEmailLoaded: true
      };
    case CHANGEEMAIL_FAIL:
      return {
        ...state,
        changeEmailError: action.error,
        changeEmailLoading: false,
        changeEmailLoaded: false,
      };
    case CHANGEPHONE:
      return {
        ...state,
        changePhoneLoading: true,
      };
    case CHANGEPHONE_SUCCESS:
      return {
        ...state,
        user: action.result,
        changePhoneLoading: false,
        changePhoneLoaded: true
      };
    case CHANGEPHONE_FAIL:
      return {
        ...state,
        changePhoneError: action.error,
        changePhoneLoading: false,
        changePhoneLoaded: false,
      };
    case BECOMEADVISOR:
      return {
        ...state,
      };
    case BECOMEADVISOR_SUCCESS:
      return {
        ...state,
        user: action.result,
      };
    case BECOMEADVISOR_FAIL:
      return {
        ...state,
        becomeadvisorfail: action.error
      };
    case UPDATEPROFILE:
      return {
        ...state,
      };
    case UPDATEPROFILE_SUCCESS:
      return {
        ...state,
        user: action.result,
      };
    case UPDATEPROFILE_FAIL:
      return {
        ...state,
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.auth && globalState.auth.loaded;
}

export function load() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get('/loadAuth')
  };
}

export function login(email, password) {
  return {
    types: [LOGIN, LOGIN_SUCCESS, LOGIN_FAIL],
    promise: (client) => client.post('/login', {
      data: {
        email: email,
        password: password
      }
    })
  };
}

export function logout() {
  return {
    types: [LOGOUT, LOGOUT_SUCCESS, LOGOUT_FAIL],
    promise: (client) => client.get('/logout')
  };
}

export function create(userBodyObj) {
  return {
    types: [CREATE, CREATE_SUCCESS, CREATE_FAIL],
    promise: (client) => client.post('/users/signup/', {data: {...userBodyObj}})
  };
}

export function checkEmailAvailability(email) {
  return {
    types: [CHECKEMAIL, CHECKEMAIL_SUCCESS, CHECKEMAIL_FAIL],
    promise: (client) => client.get('/users/getUserByEmail/' + email)
  };
}

export function checkEmailConfirmation(token) {
  return {
    types: [CHECKEMAILCONFIRMATION, CHECKEMAILCONFIRMATION_SUCCESS, CHECKEMAILCONFIRMATION_FAIL],
    promise: (client) => client.get('/users/checkEmailConfirmation?token=' + token)
  };
}

export function resendEmailConfirmation() {
  return {
    types: [RESENDEMAILCONFIRMATION, RESENDEMAILCONFIRMATION_SUCCESS, RESENDEMAILCONFIRMATION_FAIL],
    promise: (client) => client.get('/users/resendEmailConfirmation')
  };
}

export function changePassword(passwordObj) {
  return {
    types: [CHANGEPASSWORD, CHANGEPASSWORD_SUCCESS, CHANGEPASSWORD_FAIL],
    promise: (client) => client.put('/users/changePassword', {data: {...passwordObj}})
  };
}

export function forgotPassword(email) {
  return {
    types: [FORGOTPASSWORD, FORGOTPASSWORD_SUCCESS, FORGOTPASSWORD_FAIL],
    promise: (client) => client.get('/users/forgotPassword?email=' + email)
  };
}

export function checkForgotPassword(token) {
  return {
    types: [CHECKFORGOTPASSWORD, CHECKFORGOTPASSWORD_SUCCESS, CHECKFORGOTPASSWORD_FAIL],
    promise: (client) => client.get('/users/checkForgotPassword?token=' + token)
  };
}

export function forgotPasswordChange(token, passwordObj) {
  return {
    types: [FORGOTPASSWORDCHANGE, FORGOTPASSWORDCHANGE_SUCCESS, FORGOTPASSWORDCHANGE_FAIL],
    promise: (client) => client.put('/users/forgotPasswordChange', {data: {token, ...passwordObj}})
  };
}

export function confirmPin(pin) {
  return {
    types: [CONFIRMPIN, CONFIRMPIN_SUCCESS, CONFIRMPIN_FAIL],
    promise: (client) => client.put('/users/confirmPin', {data: {pin}})
  };
}

export function changeEmail(email) {
  return {
    types: [CHANGEEMAIL, CHANGEEMAIL_SUCCESS, CHANGEEMAIL_FAIL],
    promise: (client) => client.put('/users/changeEmail', {data: {newEmail: email}})
  };
}

export function changePhone(phone) {
  return {
    types: [CHANGEPHONE, CHANGEPHONE_SUCCESS, CHANGEPHONE_FAIL],
    promise: (client) => client.put('/users/changePhone', {data: {newPhoneNumber: phone}})
  };
}

export function becomeAdvisor(rate) {
  return {
    types: [BECOMEADVISOR, BECOMEADVISOR_SUCCESS, BECOMEADVISOR_FAIL],
    promise: (client) => client.put('/users/becomeAdvisor', {data: {rate: rate}})
  };
}

export function updateProfile(object) {
  return {
    types: [UPDATEPROFILE, UPDATEPROFILE_SUCCESS, UPDATEPROFILE_FAIL],
    promise: (client) => client.put('/users/updateProfile', {data: {...object}})
  };
}
