const LOADGLOBALCALENDAR = 'redux-example/profile/LOADGLOBALCALENDAR';
const LOADGLOBALCALENDAR_SUCCESS = 'redux-example/profile/LOADGLOBALCALENDAR_SUCCESS';
const LOADGLOBALCALENDAR_FAIL = 'redux-example/profile/LOADGLOBALCALENDAR_FAIL';

const initialState = {
  loaded: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOADGLOBALCALENDAR:
      return {
        ...state,
        loading: true
      };
    case LOADGLOBALCALENDAR_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        globalCalendar: action.result
      };
    case LOADGLOBALCALENDAR_FAIL:
      return {
        ...state,
        user: null,
        loading: false,
        loaded: false,
        error: action.error
      };
    default:
      return state;
  }
}

export function loadGlobalCalendar() {
  return {
    types: [LOADGLOBALCALENDAR, LOADGLOBALCALENDAR_SUCCESS, LOADGLOBALCALENDAR_FAIL],
    promise: (client) => client.get('/globalCalendar/')
  };
}
