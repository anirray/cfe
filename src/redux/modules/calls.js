const LOAD = 'cognito/calls/LOAD';
const LOAD_SUCCESS = 'cognito/calls/LOAD_SUCCESS';
const LOAD_FAIL = 'cognito/calls/LOAD_FAIL';

const initialState = {
  calls: [],
  loaded: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        calls: action.result
      };
    case LOAD_FAIL:
      return {
        ...state,
        calls: null,
        loading: false,
        loaded: false,
        error: action.error
      };
    default:
      return state;
  }
}

export function loadCalls() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get('/calls/load')
  };
}
