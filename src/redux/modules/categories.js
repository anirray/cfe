const LOAD = 'redux-example/categories/LOAD';
const LOAD_SUCCESS = 'redux-example/categories/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/categories/LOAD_FAIL';
const ADDCATEGORY = 'redux-example/categories/ADDCATEGORY';
const ADDCATEGORY_SUCCESS = 'redux-example/categories/ADDCATEGORY_SUCCESS';
const ADDCATEGORY_FAIL = 'redux-example/categories/ADDCATEGORY_FAIL';
const EDIT_START = 'redux-example/categories/EDIT_START';
const EDIT_STOP = 'redux-example/categories/EDIT_STOP';
const SAVE = 'redux-example/categories/SAVE';
const SAVE_SUCCESS = 'redux-example/categories/SAVE_SUCCESS';
const SAVE_FAIL = 'redux-example/categories/SAVE_FAIL';

const initialState = {
  categories: [],
  editing: {},
  loaded: false,
  saveError: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        categories: action.result
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    case ADDCATEGORY:
      return {
        ...state,
        loading: true
      };
    case ADDCATEGORY_SUCCESS:
      return {
        ...state,
        loading: false,
        categories: state.categories.concat(action.result)
      };
    case ADDCATEGORY_FAIL:
      return {
        ...state,
        loading: false,
        error: action.error
      };
    case EDIT_START:
      return {
        ...state,
        editing: {
          ...state.editing,
          [action.id]: true
        }
      };
    case EDIT_STOP:
      return {
        ...state,
        editing: {
          ...state.editing,
          [action.id]: false
        }
      };
    case SAVE:
      return state; // 'saving' flag handled by redux-form
    case SAVE_SUCCESS:
      let categories = [...state.categories];
      categories = categories.map((category) => {
        if (category.id === action.result.id) {
          return action.result;
        }
        return category;
      });
      return {
        ...state,
        categories: categories,
        editing: {
          ...state.editing,
          [action.id]: false
        },
        saveError: {
          ...state.saveError,
          [action.id]: null
        }
      };
    case SAVE_FAIL:
      return typeof action.error === 'string' ? {
        ...state,
        saveError: {
          ...state.saveError,
          [action.id]: action.error
        }
      } : state;
    default:
      return state;
  }
}

export function loadCategories() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get('/categories/load/')
  };
}

export function addCategory(category) {
  console.log(category, 'reducer called');
  return {
    types: [ADDCATEGORY, ADDCATEGORY_SUCCESS, ADDCATEGORY_FAIL],
    promise: (client) => client.post('/categories/add', {
      data: {name: category}
    })
  };
}

export function save(category) {
  return {
    types: [SAVE, SAVE_SUCCESS, SAVE_FAIL],
    id: category.id,
    promise: (client) => client.post('/categories/update', {
      data: category
    })
  };
}

export function editStart(id) {
  return { type: EDIT_START, id };
}

export function editStop(id) {
  return { type: EDIT_STOP, id };
}
