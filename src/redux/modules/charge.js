

const LOAD = 'redux-example/charge/LOAD';
const LOAD_SUCCESS = 'redux-example/charge/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/charge/LOAD_FAIL';
const CHARGELOAD = 'redux-example/charge/CHARGELOAD';
const CHARGELOAD_SUCCESS = 'redux-example/charge/CHARGELOAD_SUCCESS';
const CHARGELOAD_FAIL = 'redux-example/charge/CHARGELOAD_FAIL';

const initialState = {
  loaded: false,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        user: action.result
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    case CHARGELOAD:
      return {
        ...state,
        loading: true,
      };
    case CHARGELOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: false,
        charge: action.charge
      };
    case CHARGELOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    default:
      return state;
  }
}

export function chargeCard(token, amount, times, user, advisor) {
  return {
    types: [CHARGELOAD, CHARGELOAD_SUCCESS, CHARGELOAD_FAIL],
    promise: (client) => client.post('/chargeCard', {data: {token, amount, times, user, advisor}})
  };
}


export function checkCalendar(slotObj) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get('/checkCalendar', {data: {...slotObj}})
  };
}
