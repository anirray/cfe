const LOAD = 'cognito/orders/LOAD';
const LOAD_SUCCESS = 'cognito/orders/LOAD_SUCCESS';
const LOAD_FAIL = 'cognito/orders/LOAD_FAIL';

const initialState = {
  orders: [],
  loaded: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        orders: action.result
      };
    case LOAD_FAIL:
      return {
        ...state,
        orders: null,
        loading: false,
        loaded: false,
        error: action.error
      };
    default:
      return state;
  }
}

export function loadOrders() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get('/orders/load')
  };
}
