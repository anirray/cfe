const LOAD = 'redux-example/profile/LOAD';
const LOAD_SUCCESS = 'redux-example/profile/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/profile/LOAD_FAIL';
const LOADCALENDAR = 'redux-example/profile/LOADCALENDAR';
const LOADCALENDAR_SUCCESS = 'redux-example/profile/LOADCALENDAR_SUCCESS';
const LOADCALENDAR_FAIL = 'redux-example/profile/LOADCALENDAR_FAIL';
const ADDAVAIL = 'redux-example/profile/ADDAVAIL';
const ADDAVAIL_SUCCESS = 'redux-example/profile/ADDAVAIL_SUCCESS';
const ADDAVAIL_FAIL = 'redux-example/profile/ADDAVAIL_FAIL';

const initialState = {
  loaded: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        user: action.result
      };
    case LOAD_FAIL:
      return {
        ...state,
        user: null,
        loading: false,
        loaded: false,
        error: action.error
      };
    case LOADCALENDAR:
      return {
        ...state,
        loading: true
      };
    case LOADCALENDAR_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        calendar: action.result
      };
    case LOADCALENDAR_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    case ADDAVAIL:
      return {
        ...state,
        loading: true
      };
    case ADDAVAIL_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        calendar: action.result
      };
    case ADDAVAIL_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    default:
      return state;
  }
}

export function load(userId) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get('/users/getUser/' + userId)
  };
}

export function loadUserCalendar(userId) {
  return {
    types: [LOADCALENDAR, LOADCALENDAR_SUCCESS, LOADCALENDAR_FAIL],
    promise: (client) => client.get('/users/getUserCalendar/' + userId)
  };
}

export function setAvail(avail, user) {
  return {
    types: [ADDAVAIL, ADDAVAIL_SUCCESS, ADDAVAIL_FAIL],
    promise: (client) => client.post('/users/addAvail', {data: {avail, user}})
  };
}
