import { combineReducers } from 'redux';
import multireducer from 'multireducer';
import { routerStateReducer } from 'redux-router';

import advisors from './advisors';
import auth from './auth';
import categories from './categories';
import calls from './calls';
import counter from './counter';
import {reducer as form} from 'redux-form';
import info from './info';
import profile from './profile';
import orders from './orders';
import widgets from './widgets';
import charge from './charge';
import calendar from './calendar';

export default combineReducers({
  router: routerStateReducer,
  advisors,
  auth,
  categories,
  calls,
  form,
  charge,
  multireducer: multireducer({
    counter1: counter,
    counter2: counter,
    counter3: counter
  }),
  orders,
  info,
  calendar,
  profile,
  widgets,
});
