import React from 'react';
import {IndexRoute, Route} from 'react-router';
import { isLoaded as isAuthLoaded, load as loadAuth } from 'redux/modules/auth';
import {
    About,
    Account,
    AccountSettings,
    AdvisorList,
    AdvisorSettings,
    Admin,
    App,
    BecomeAdvisor,
    Calls,
    CalendarSettings,
    Categories,
    ChangePassword,
    Chat,
    ConfirmPassword,
    ConfirmUser,
    Dashboard,
    Home,
    Login,
    LoginSuccess,
    ForgotPassword,
    NotFound,
    Orders,
    Profile,
    ProfileDisplay,
    Roles,
    Scheduler,
    Signup,
    Survey,
    Widgets,
  } from 'containers';

export default (store) => {
  const requireLogin = (nextState, replaceState, cb) => {
    function checkAuth() {
      const { auth: { user }} = store.getState();
      if (!user) {
        // oops, not logged in, so can't be here!
        replaceState(null, '/');
      }
      cb();
    }

    if (!isAuthLoaded(store.getState())) {
      store.dispatch(loadAuth()).then(checkAuth);
    } else {
      checkAuth();
    }
  };

  const requireAdmin = (nextState, replaceState, cb) => {
    function checkAuth() {
      const { auth: { user }} = store.getState();
      if (!user || user.AccountTypeId !== 3) {
        // Not an admin, redirect back to home
        replaceState(null, '/');
      }
      cb();
    }

    if (!isAuthLoaded(store.getState())) {
      store.dispatch(loadAuth()).then(checkAuth);
    } else {
      checkAuth();
    }
  };

  const loggedInReroute = (nextState, replaceState, cb) => {
    function checkAuth() {
      const { auth: { user }} = store.getState();
      if (user) {
        // Logged in so reroute to the user's profile page
        replaceState(nextState, `/profile/${user.id}`);
      }
      cb();
    }

    if (!isAuthLoaded(store.getState())) {
      store.dispatch(loadAuth()).then(checkAuth);
    } else {
      checkAuth();
    }
  };

  /**
   * Please keep routes in alphabetical order
   */
  return (
    <Route path="/" component={App}>
      { /* Home (main) route */ }
      <IndexRoute component={Home}/>

      { /* Routes requiring login */ }
      <Route onEnter={requireLogin}>
        <Route path="account" component={Account}>
          <Route path="accountSettings" component={AccountSettings} />
          <Route path="advisorSettings" component={AdvisorSettings} />
          <Route path="becomeAdvisor" component={BecomeAdvisor} />
          <Route path="calendarSettings" component={CalendarSettings} />
          <Route path="changePassword" component={ChangePassword} />
          <Route path="profileDisplay" component={ProfileDisplay} />
          <Route path="scheduler" component={Scheduler} />
        </Route>
        <Route path="calls" component={Calls} />
        <Route path="dashboard" component={Dashboard} />
        <Route path="orders" component={Orders} />
        <Route path="chat" component={Chat}/>
        <Route path="loginSuccess" component={LoginSuccess}/>
      </Route>

      { /* Admin Routes, Requires User Account to Have Administrator Role*/ }
      <Route onEnter={requireAdmin}>
        <Route path="admin" component={Admin}>
          <Route path="categories" component={Categories}/>
          <Route path="roles" component={Roles}/>
        </Route>
      </Route>

      { /* Routes */ }
      <Route path="about" component={About}/>
      <Route path="advisorlist" component={AdvisorList}/>
      <Route path="confirmpassword" onEnter={loggedInReroute} component={ConfirmPassword}/>
      <Route path="confirmuser" component={ConfirmUser}/>
      <Route path="forgotpassword" component={ForgotPassword}/>
      <Route path="login" onEnter={loggedInReroute} component={Login}/>
      <Route path="profile/:id" component={Profile}/>
      <Route path="signup" onEnter={loggedInReroute} component={Signup}/>
      <Route path="survey" component={Survey}/>
      <Route path="widgets" component={Widgets}/>

      { /* Catch all route */ }
      <Route path="*" component={NotFound} status={404} />
    </Route>
  );
};
